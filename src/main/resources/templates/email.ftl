<!DOCTYPE html>
<html>

<head>
  <title>Page Title</title>
  <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
  <style>
    .body {
      font-family: 'Roboto', sans-serif;
      padding: 0 200px;
      background-color: #eee;
    }

    table {
      width: 100%;
      background-color: white;
      color: rgb(20, 20, 20);
      text-align: center;
    }

    .header {
      background-color: rgb(0, 196, 204);
      height: 100px;
      font-size: 28px;
      color: white;
    }

    .title {
      text-transform: uppercase;
      text-align: center;
      height: 70px;
      font-size: 24px;
      font-weight: bold;
    }

    .body-general {
      text-align: center;
      font-size: 18px;
    }

    .body-credentials {
      padding: 10px 0;
    }

    .td-title {
      text-align: right;
      width: 50%;
    }

    .td-content {
      text-align: left;
      width: 50%;
    }

    .note {
      font-size: 12px;
    }

    span {
      font-weight: bold;
    }

    .image {
      padding: 15px 0;
    }

    .redirect {
      height: 130px;
    }

    .redirect>a {
      background-color: #f5774e;
      color: beige;
      padding: 20px;
      text-decoration: none;
      border-radius: 10px;
    }

    .redirect>a:hover {
      background-color: #eb5320;
      color: white;
      text-decoration: none;
    }

    .rights {
      background-color: #363636;
      color: #f0f0f0;
      font-size: 14px;
      height: 80px;
    }
  </style>
</head>

<body class="body">
  <table>
    <tr>
      <td class="header">Confirmación de correo electrónico</td>
    </tr>
    <tr>
      <td class="title">BIENVENIDO</td>
    </tr>
    <tr>
      <td class="body-general">Hola ${firstName} ${lastName}, se ha creado un usuario con su cuenta de correo
        electrónico con la
        que podrá ingresar al portal de proturvac web.</td>
    </tr>
    <tr>
      <td class="body-credentials">Sus credeciales de acceso son:</td>
    </tr>
    <tr>
      <td class="body-credentials">
        <table>
          <tr>
            <td class="td-title">
              Usuario:
            </td>
            <td class="td-content"><span>${email}</span></td>
          </tr>
          <tr>
            <td class="td-title">
              Contraseña:
            </td>
            <td class="td-content"><span>${password}</span></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td class="note"><span>NOTA:</span> Al ingresar por primera ves el portal web le va a solicitar la contraseña
        temporal señalada
        anteriormente, una ves cambiada la contraseña, la contraseña temporal no funcionará. En caso de no recordar su
        contraseña tendrá que contactar con su administrador del portal para generar su contraseña temporal nuevamente.
      </td>
    </tr>
    <tr>
      <td class="image"><img src="https://k60.kn3.net/7/0/A/C/7/C/A0A.png"></td>
    </tr>
    <tr>
      <td class="redirect"><a href="${url}">Iniciar sesión</a></td>
    </tr>
    <tr>
      <td class="rights">© 2019 All Rights Reserved</td>
    </tr>
  </table>
</body>

</html>