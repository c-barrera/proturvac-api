/**
 * 
 */
package com.cisbarey.proturvac.config;

import java.io.Serializable;

import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author Stratis
 *
 */
public class CustomPermissionEvaluator implements PermissionEvaluator {

	@Override
	public boolean hasPermission(Authentication authentication, Object permission, Object privilege) {
		if ((authentication == null) || (permission == null) || !(privilege instanceof String)) {
			return false;
		}

		return hasPrivilege(authentication, permission.toString().toUpperCase(), privilege.toString().toUpperCase());
	}

	@Override
	public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType,
			Object permission) {
		if ((authentication == null) || (targetType == null) || !(permission instanceof String)) {
			return false;
		}
		return hasPrivilege(authentication, targetType.toUpperCase(), permission.toString().toUpperCase());
	}

	private boolean hasPrivilege(Authentication auth, String permission, String privilege) {
		for (GrantedAuthority grantedAuth : auth.getAuthorities()) {
			if (grantedAuth.getAuthority().startsWith(permission) && grantedAuth.getAuthority().endsWith(privilege)) {
				return true;
			}
		}
		return false;
	}

}
