package com.cisbarey.proturvac.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;

import com.cisbarey.proturvac.util.IFile;

@Configuration
@MapperScan("com.cisbarey.proturvac.mapper")
@PropertySource("classpath:general.properties")
@EnableConfigurationProperties({IFile.StorageProperties.class})
public class AppConfig {
	
	@Bean
	@Primary
	public FreeMarkerConfigurationFactoryBean getFreeMarkerConfiguration() {
		FreeMarkerConfigurationFactoryBean bean = new FreeMarkerConfigurationFactoryBean();
		bean.setPreferFileSystemAccess(Boolean.FALSE);
		bean.setTemplateLoaderPath("classpath:/templates/");
		return bean;
	}

}
