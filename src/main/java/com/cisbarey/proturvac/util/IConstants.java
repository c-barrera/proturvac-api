/**
 * 
 */
package com.cisbarey.proturvac.util;

/**
 * @author Stratis
 *
 */
public interface IConstants {
	static interface Success {
		static String INSERT = "El registro fue agregado satisfactoriamente!";
		static String UPDATE = "El registro fue modificado satisfactoriamente!";
		static String DELETE = "El registro fue eliminado satisfactoriamente!";
	}
	
	static interface Errors  {
		static String USER_INVALID = "Usuario o contraseña inválidos!";
		static String LIST_EMPTY = "No se encontraron registros.";
		static String NOT_FOUND = "No se encontró el registro.";
		static String DUPLICATE_KEY = "Registro existente!";
	}
	
	static interface Hash {
		static String ALGORITHM = "PBKDF2WithHmacSHA1";
		static int INTERATIONS = 1000;
		static int KEY_LENGTH = 64 * 8;
	}
	
	static interface Password {
		static int LENGTH = 10;
		static String DICTIONARY = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	}
	
	static interface File {
		static String FILE_NAME_PRODUCT_PATTERN = "P_{0}.{1}";
		static String FULL_NAME_PRODUCT_PATTERN = "products{0}{1}";
	}
}
