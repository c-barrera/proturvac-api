/**
 * 
 */
package com.cisbarey.proturvac.util;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author cisbarey
 *
 */
public interface IFile {
	
	@ConfigurationProperties(prefix = "file")
	static class StorageProperties {
		
		private String uploadDir;

		/**
		 * @return the uploadDir
		 */
		public String getUploadDir() {
			return uploadDir;
		}

		/**
		 * @param uploadDir the uploadDir to set
		 */
		public void setUploadDir(String uploadDir) {
			this.uploadDir = uploadDir;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("StorageProperties [uploadDir=");
			builder.append(uploadDir);
			builder.append("]");
			return builder.toString();
		}

	}

}
