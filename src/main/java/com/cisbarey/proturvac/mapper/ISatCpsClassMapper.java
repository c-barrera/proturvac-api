/**
 * 
 */
package com.cisbarey.proturvac.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.cisbarey.proturvac.domain.SatCpsClass;
import com.cisbarey.proturvac.exception.ProturvacException;

/**
 * <p>
 * Interface para el manejo de sentencias a la base de datos.
 * </p>
 * 
 * @author Stratis
 *
 */
@Mapper
public interface ISatCpsClassMapper extends IMapper<SatCpsClass> {
	
	Integer findId(Map<String, String> params) throws ProturvacException;

}
