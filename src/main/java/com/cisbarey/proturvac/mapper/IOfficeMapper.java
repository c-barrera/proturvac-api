/**
 * 
 */
package com.cisbarey.proturvac.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.cisbarey.proturvac.domain.Office;

/**
 * <p>
 * Interface para el manejo de sentencias a la base de datos.
 * </p>
 * 
 * @author Stratis
 *
 */
@Mapper
public interface IOfficeMapper extends IMapper<Office> {

	List<Office> findByCompany(int company);
}
