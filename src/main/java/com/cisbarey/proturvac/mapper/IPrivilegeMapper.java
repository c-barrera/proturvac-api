/**
 * 
 */
package com.cisbarey.proturvac.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.cisbarey.proturvac.domain.Privilege;

/**
 * <p>
 * Interface para el manejo de sentencias a la base de datos.
 * </p>
 * 
 * @author Stratis
 */
@Mapper
public interface IPrivilegeMapper {
	
	/**
	 * <p>
	 * M&eacute;todo para agregar un registro a la base de datos.
	 * </p>
	 * 
	 * @param param Objeto gen&eacute;rico.
	 */
	public void insert(Privilege param);
	
	/**
	 * <p>
	 * M&eacute;todo para eliminar un registro de la base de datos.
	 * </p>
	 * 
	 * @param id Identificador.
	 */
	public void delete(Privilege param);

}
