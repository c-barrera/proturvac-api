/**
 * 
 */
package com.cisbarey.proturvac.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.cisbarey.proturvac.domain.Menu;

/**
 * <p>
 * Interface para el manejo de sentencias a la base de datos.
 * </p>
 * @author Stratis
 */
@Mapper
public interface IMenuMapper extends IMapper<Menu> {
	
	/**
	 * <p>M&eacute;todo que busca el listado de men&uacute; por perfil.</p>
	 * @param id El id del perfil.
	 * @return
	 */
	List<Menu> findByProfile(int id);
	
	/**
	 * <p>M&eacute;todo que busca los privilegios (Men&uacute;) por perfil.</p>
	 * 
	 * @param id Identificador del perfil.
	 * @return Listado de privilegios.
	 */
	List<String> findPrivileges(int id);
}
