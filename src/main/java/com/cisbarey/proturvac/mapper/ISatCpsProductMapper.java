/**
 * 
 */
package com.cisbarey.proturvac.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.cisbarey.proturvac.domain.SatCpsProduct;
import com.cisbarey.proturvac.domain.SearchSKU;

/**
 * <p>
 * Interface para el manejo de sentencias a la base de datos.
 * </p>
 * 
 * @author Stratis
 *
 */
@Mapper
public interface ISatCpsProductMapper extends IMapper<SatCpsProduct> {
	
	/**
	 * 
	 * @param params
	 * @return
	 */
	Integer findId(Map<String, String> params);

	/**
	 * 
	 * @param text
	 * @return
	 */
	List<SearchSKU> searchSKU(String text);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	SearchSKU findSKUByIdProduct(int id);
}
