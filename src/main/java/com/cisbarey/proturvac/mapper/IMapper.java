/**
 * 
 */
package com.cisbarey.proturvac.mapper;

import java.util.List;

/**
 * <p>
 * Interface gen&eacute;rica para el manejo de sentencias a la base de datos.
 * </p>
 * 
 * @author Stratis
 */
public interface IMapper<T> {
	
	/**
	 * <p>
	 * M&eacute;todo gen&eacute;rico para obtener un listado de resultados.
	 * </p>
	 * 
	 * @return Listado de resultados.
	 */
	public List<T> findList();

	/**
	 * <p>
	 * M&eacute;todo que obtiene un registro de acuerdo al <strong>ID</strong>
	 * enviado como par&aacute;metro.
	 * </p>
	 * 
	 * @param id Identificador.
	 * @return Objeto.
	 */
	public T findOne(int id);

	/**
	 * <p>
	 * M&eacute;todo para agregar un registro a la base de datos.
	 * </p>
	 * 
	 * @param param Objeto gen&eacute;rico.
	 */
	public void insert(T param);

	/**
	 * <p>
	 * M&eacute;todo para modificar un registro de la base de datos.
	 * </p>
	 * 
	 * @param param Objeto gen&eacute;rico.
	 */
	public void update(T param);

	/**
	 * <p>
	 * M&eacute;todo para eliminar un registro de la base de datos.
	 * </p>
	 * 
	 * @param id Identificador.
	 */
	public void delete(int id);

}
