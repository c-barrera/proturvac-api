/**
 * 
 */
package com.cisbarey.proturvac.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.cisbarey.proturvac.domain.Order;

/**
 * <p>
 * Interface para el manejo de sentencias a la base de datos.
 * </p>
 * 
 * @author Stratis
 *
 */
@Mapper
public interface IOrderMapper extends IMapper<Order> {
	
}
