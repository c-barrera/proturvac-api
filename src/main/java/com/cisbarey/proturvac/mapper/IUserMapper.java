/**
 * 
 */
package com.cisbarey.proturvac.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.cisbarey.proturvac.domain.User;

/**
 * <p>
 * Interface para el manejo de sentencias a la base de datos.
 * </p>
 * 
 * @author Stratis
 *
 */
@Mapper
public interface IUserMapper extends IMapper<User> {

	int findID(String email);
	void insertEmployee(User user);
	void deleteEmployee(int fkUser);
}
