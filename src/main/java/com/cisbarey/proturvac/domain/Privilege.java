/**
 * 
 */
package com.cisbarey.proturvac.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Stratis
 *
 */
public class Privilege implements Serializable {

	/**
	 * Generado por el IDE.
	 */
	private static final long serialVersionUID = -881572343156767463L;
	
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private Integer fkProfile;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private Integer fkMenu;

	/**
	 * Constructor default.
	 */
	public Privilege() {
		super();
	}

	/**
	 * @param fkProfile
	 * @param fkMenu
	 */
	public Privilege(Integer fkProfile, Integer fkMenu) {
		super();
		this.fkProfile = fkProfile;
		this.fkMenu = fkMenu;
	}

	/**
	 * @return the fkProfile
	 */
	public Integer getFkProfile() {
		return fkProfile;
	}

	/**
	 * @param fkProfile the fkProfile to set
	 */
	public void setFkProfile(Integer fkProfile) {
		this.fkProfile = fkProfile;
	}

	/**
	 * @return the fkMenu
	 */
	public Integer getFkMenu() {
		return fkMenu;
	}

	/**
	 * @param fkMenu the fkMenu to set
	 */
	public void setFkMenu(Integer fkMenu) {
		this.fkMenu = fkMenu;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Privilege [fkProfile=");
		builder.append(fkProfile);
		builder.append(", fkMenu=");
		builder.append(fkMenu);
		builder.append("]");
		return builder.toString();
	}

}
