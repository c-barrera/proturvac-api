/**
 * 
 */
package com.cisbarey.proturvac.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author cisbarey
 *
 */
public class SearchSKU implements Serializable {
	
	/**
	 * Generado por el IDE.
	 */
	private static final long serialVersionUID = -5860047578255912254L;
	
	private Integer id;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String sku;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String typeDescription;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String divisionDescription;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String groupDescription;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String classDescription;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String productDescription;

	/**
	 * Constructor default.
	 */
	public SearchSKU() {
		super();
	}

	/**
	 * @param id
	 * @param sku
	 * @param typeDescription
	 * @param divisionDescription
	 * @param groupDescription
	 * @param classDescription
	 * @param productDescription
	 */
	public SearchSKU(Integer id, String sku, String typeDescription, String divisionDescription,
			String groupDescription, String classDescription, String productDescription) {
		super();
		this.id = id;
		this.sku = sku;
		this.typeDescription = typeDescription;
		this.divisionDescription = divisionDescription;
		this.groupDescription = groupDescription;
		this.classDescription = classDescription;
		this.productDescription = productDescription;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the sku
	 */
	public String getSku() {
		return sku;
	}

	/**
	 * @param sku the sku to set
	 */
	public void setSku(String sku) {
		this.sku = sku;
	}

	/**
	 * @return the typeDescription
	 */
	public String getTypeDescription() {
		return typeDescription;
	}

	/**
	 * @param typeDescription the typeDescription to set
	 */
	public void setTypeDescription(String typeDescription) {
		this.typeDescription = typeDescription;
	}

	/**
	 * @return the divisionDescription
	 */
	public String getDivisionDescription() {
		return divisionDescription;
	}

	/**
	 * @param divisionDescription the divisionDescription to set
	 */
	public void setDivisionDescription(String divisionDescription) {
		this.divisionDescription = divisionDescription;
	}

	/**
	 * @return the groupDescription
	 */
	public String getGroupDescription() {
		return groupDescription;
	}

	/**
	 * @param groupDescription the groupDescription to set
	 */
	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}

	/**
	 * @return the classDescription
	 */
	public String getClassDescription() {
		return classDescription;
	}

	/**
	 * @param classDescription the classDescription to set
	 */
	public void setClassDescription(String classDescription) {
		this.classDescription = classDescription;
	}

	/**
	 * @return the productDescription
	 */
	public String getProductDescription() {
		return productDescription;
	}

	/**
	 * @param productDescription the productDescription to set
	 */
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SearchSKU [id=");
		builder.append(id);
		builder.append(", sku=");
		builder.append(sku);
		builder.append(", typeDescription=");
		builder.append(typeDescription);
		builder.append(", divisionDescription=");
		builder.append(divisionDescription);
		builder.append(", groupDescription=");
		builder.append(groupDescription);
		builder.append(", classDescription=");
		builder.append(classDescription);
		builder.append(", productDescription=");
		builder.append(productDescription);
		builder.append("]");
		return builder.toString();
	}

}
