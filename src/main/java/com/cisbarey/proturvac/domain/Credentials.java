/**
 * 
 */
package com.cisbarey.proturvac.domain;

import java.io.Serializable;

/**
 * @author Stratis
 *
 */
public class Credentials implements Serializable {
	
	/**
	 * Generado por el IDE.
	 */
	private static final long serialVersionUID = -6050971501261718940L;
	
	private String username;
	private String password;

	/**
	 * Constructor default.
	 */
	public Credentials() {
		super();
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Credentials [username=");
		builder.append(username);
		builder.append(", password=");
		builder.append(password);
		builder.append("]");
		return builder.toString();
	}

}
