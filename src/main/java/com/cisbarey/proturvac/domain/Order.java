/**
 * 
 */
package com.cisbarey.proturvac.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author cisbarey
 *
 */
public class Order extends OperationCommon {

	/**
	 * Generado por el IDE.
	 */
	private static final long serialVersionUID = -2415169502285559100L;
	
	private Integer id;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private Integer quantity;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private Double price;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String image;
	
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private List<Product> products;

	/**
	 * Constructor default.
	 */
	public Order() {
		super();
	}

	/**
	 * @param id
	 * @param quantity
	 * @param price
	 * @param image
	 */
	public Order(Integer id, Integer quantity, Double price, String image) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.price = price;
		this.image = image;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(Double price) {
		this.price = price;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * @return the products
	 */
	public List<Product> getProducts() {
		return products;
	}

	/**
	 * @param products the products to set
	 */
	public void setProducts(List<Product> products) {
		this.products = products;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Order [id=");
		builder.append(id);
		builder.append(", quantity=");
		builder.append(quantity);
		builder.append(", price=");
		builder.append(price);
		builder.append(", image=");
		builder.append(image);
		builder.append(", products=");
		builder.append(products);
		builder.append("]");
		return builder.toString();
	}
	
}
