/**
 * 
 */
package com.cisbarey.proturvac.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Stratis
 *
 */
public class Menu extends Common {

	/**
	 * Generado por el IDE.
	 */
	private static final long serialVersionUID = -8743621368379189426L;
	
	private Integer id;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String name;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String description;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String path;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private Integer order;

	/**
	 * Constructor default.
	 */
	public Menu() {
		super();
	}

	/**
	 * @param id
	 * @param name
	 * @param description
	 * @param path
	 * @param order
	 */
	public Menu(Integer id, String name, String description, String path, Integer order) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.path = path;
		this.order = order;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the order
	 */
	public Integer getOrder() {
		return order;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder(Integer order) {
		this.order = order;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Menu [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", description=");
		builder.append(description);
		builder.append(", path=");
		builder.append(path);
		builder.append(", order=");
		builder.append(order);
		builder.append("]");
		return builder.toString();
	}
	
}
