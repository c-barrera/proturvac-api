/**
 * 
 */
package com.cisbarey.proturvac.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Stratis
 *
 */
public class User extends Common {
	
	/**
	 * Generado por el IDE.
	 */
	private static final long serialVersionUID = 6180397361926522587L;
	
	private Integer id;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String firstName;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String lastName;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String email;
	@JsonIgnore
	private String password;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private Boolean changePassword;
	
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private Profile profile;
	
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String token;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private Office office;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private Company company;

	/**
	 * Constructor default.
	 */
	public User() {
		super();
	}

	/**
	 * @param id
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param password
	 * @param changePassword
	 * @param profile
	 */
	public User(Integer id, String firstName, String lastName, String email, String password, Boolean changePassword,
			Profile profile) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.changePassword = changePassword;
		this.profile = profile;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the changePassword
	 */
	public Boolean getChangePassword() {
		return changePassword;
	}

	/**
	 * @param changePassword the changePassword to set
	 */
	public void setChangePassword(Boolean changePassword) {
		this.changePassword = changePassword;
	}

	/**
	 * @return the profile
	 */
	public Profile getProfile() {
		return profile;
	}

	/**
	 * @param profile the profile to set
	 */
	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the office
	 */
	public Office getOffice() {
		return office;
	}

	/**
	 * @param office the office to set
	 */
	public void setOffice(Office office) {
		this.office = office;
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("User [id=");
		builder.append(id);
		builder.append(", firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", email=");
		builder.append(email);
		builder.append(", password=");
		builder.append(password);
		builder.append(", changePassword=");
		builder.append(changePassword);
		builder.append(", profile=");
		builder.append(profile);
		builder.append(", token=");
		builder.append(token);
		builder.append(", office=");
		builder.append(office);
		builder.append(", company=");
		builder.append(company);
		builder.append("]");
		return builder.toString();
	}
}
