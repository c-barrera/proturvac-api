/**
 * 
 */
package com.cisbarey.proturvac.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Stratis
 *
 */
public class Company extends Common {

	/**
	 * Generado por el IDE.
	 */
	private static final long serialVersionUID = -6434993346967147810L;
	
	private Integer id;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String description;

	/**
	 * Constructor default.
	 */
	public Company() {
		super();
	}

	/**
	 * @param id
	 * @param description
	 */
	public Company(Integer id, String description) {
		super();
		this.id = id;
		this.description = description;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Company [id=");
		builder.append(id);
		builder.append(", description=");
		builder.append(description);
		builder.append("]");
		return builder.toString();
	}
}
