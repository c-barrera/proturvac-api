/**
 * 
 */
package com.cisbarey.proturvac.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author cisbarey
 *
 */
public class SatCpsClass implements Serializable {

	/**
	 * Generado por el IDE.
	 */
	private static final long serialVersionUID = -171277392561713405L;
	
	private Integer id;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String code;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String description;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private Integer fkSatCpsGroup;

	/**
	 * Constructor default.
	 */
	public SatCpsClass() {
		super();
	}

	/**
	 * @param id
	 * @param code
	 * @param description
	 * @param fkSatCpsGroup
	 */
	public SatCpsClass(Integer id, String code, String description, Integer fkSatCpsGroup) {
		super();
		this.id = id;
		this.code = code;
		this.description = description;
		this.fkSatCpsGroup = fkSatCpsGroup;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the fkSatCpsGroup
	 */
	public Integer getFkSatCpsGroup() {
		return fkSatCpsGroup;
	}

	/**
	 * @param fkSatCpsGroup the fkSatCpsGroup to set
	 */
	public void setFkSatCpsGroup(Integer fkSatCpsGroup) {
		this.fkSatCpsGroup = fkSatCpsGroup;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SatCpsClass [id=");
		builder.append(id);
		builder.append(", code=");
		builder.append(code);
		builder.append(", description=");
		builder.append(description);
		builder.append(", fkSatCpsGroup=");
		builder.append(fkSatCpsGroup);
		builder.append("]");
		return builder.toString();
	}

}
