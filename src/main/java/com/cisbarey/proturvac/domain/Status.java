/**
 * 
 */
package com.cisbarey.proturvac.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Stratis
 *
 */
public class Status implements Serializable {

	/**
	 * Generado por e IDE.
	 */
	private static final long serialVersionUID = -1978470867164038747L;
	
	private Integer id;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String description;

	/**
	 * Constructor default.
	 */
	public Status() {
		super();
	}

	/**
	 * @param id
	 * @param description
	 */
	public Status(Integer id, String description) {
		super();
		this.id = id;
		this.description = description;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Status [id=");
		builder.append(id);
		builder.append(", description=");
		builder.append(description);
		builder.append("]");
		return builder.toString();
	}
}
