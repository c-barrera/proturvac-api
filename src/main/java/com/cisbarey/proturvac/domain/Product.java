/**
 * 
 */
package com.cisbarey.proturvac.domain;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author cisbarey
 *
 */
public class Product extends OperationCommon {

	/**
	 * Generado por el IDE.
	 */
	private static final long serialVersionUID = -8828002335560420985L;
	
	private Integer id;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String name;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String description;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String image;
	
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private SatCpsProduct sku;
	
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private Provider provider;
	
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private MultipartFile file;

	/**
	 * Constructor default.
	 */
	public Product() {
		super();
	}

	/**
	 * @param status
	 * @param createdAt
	 * @param updatedAt
	 */
	public Product(Status status, Date createdAt, Date updatedAt) {
		super(status, createdAt, updatedAt);
	}

	/**
	 * @param createdBy
	 * @param updatedBy
	 */
	public Product(User createdBy, User updatedBy) {
		super(createdBy, updatedBy);
	}

	/**
	 * @param id
	 * @param name
	 * @param description
	 * @param image
	 * @param sku
	 * @param provider
	 */
	public Product(Integer id, String name, String description, String image, SatCpsProduct sku, Provider provider) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.image = image;
		this.sku = sku;
		this.provider = provider;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * @return the sku
	 */
	public SatCpsProduct getSku() {
		return sku;
	}

	/**
	 * @param sku the sku to set
	 */
	public void setSku(SatCpsProduct sku) {
		this.sku = sku;
	}

	/**
	 * @return the provider
	 */
	public Provider getProvider() {
		return provider;
	}

	/**
	 * @param provider the provider to set
	 */
	public void setProvider(Provider provider) {
		this.provider = provider;
	}

	/**
	 * @return the file
	 */
	public MultipartFile getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(MultipartFile file) {
		this.file = file;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Product [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", description=");
		builder.append(description);
		builder.append(", image=");
		builder.append(image);
		builder.append(", sku=");
		builder.append(sku);
		builder.append(", provider=");
		builder.append(provider);
		builder.append("]");
		return builder.toString();
	}

}
