/**
 * 
 */
package com.cisbarey.proturvac.domain;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Stratis
 *
 */
public class Common implements Serializable {

	/**
	 * Generado por el IDE.
	 */
	private static final long serialVersionUID = -8078488180362777160L;
	
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private Status status;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private Date createdAt;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private Date updatedAt;

	/**
	 * Contructor default.
	 */
	public Common() {
		super();
	}

	/**
	 * @param status
	 * @param createdAt
	 * @param updatedAt
	 */
	public Common(Status status, Date createdAt, Date updatedAt) {
		super();
		this.status = status;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the updatedAt
	 */
	public Date getUpdatedAt() {
		return updatedAt;
	}

	/**
	 * @param updatedAt the updatedAt to set
	 */
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Common [status=");
		builder.append(status);
		builder.append(", createdAt=");
		builder.append(createdAt);
		builder.append(", updatedAt=");
		builder.append(updatedAt);
		builder.append("]");
		return builder.toString();
	}

}
