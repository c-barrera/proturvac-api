/**
 * 
 */
package com.cisbarey.proturvac.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author cisbarey
 *
 */
public class Provider extends OperationCommon {

	/**
	 * Generado por el IDE.
	 */
	private static final long serialVersionUID = 6041730967057819450L;
	
	private Integer id;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String name;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String description;

	/**
	 * Constructor default.
	 */
	public Provider() {
		super();
	}

	/**
	 * @param status
	 * @param createdAt
	 * @param updatedAt
	 */
	public Provider(Status status, Date createdAt, Date updatedAt) {
		super(status, createdAt, updatedAt);
	}

	/**
	 * @param createdBy
	 * @param updatedBy
	 */
	public Provider(User createdBy, User updatedBy) {
		super(createdBy, updatedBy);
	}

	/**
	 * @param id
	 * @param name
	 * @param description
	 */
	public Provider(Integer id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Provider [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", description=");
		builder.append(description);
		builder.append("]");
		return builder.toString();
	}

}
