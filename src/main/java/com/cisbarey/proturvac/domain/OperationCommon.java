/**
 * 
 */
package com.cisbarey.proturvac.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author cisbarey
 *
 */
public class OperationCommon extends Common {

	/**
	 * Generado por el IDE.
	 */
	private static final long serialVersionUID = -495828646186586694L;
	
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private User createdBy;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private User updatedBy;

	/**
	 * Constructor default.
	 */
	public OperationCommon() {
		super();
	}

	/**
	 * @param status
	 * @param createdAt
	 * @param updatedAt
	 */
	public OperationCommon(Status status, Date createdAt, Date updatedAt) {
		super(status, createdAt, updatedAt);
	}

	/**
	 * @param createdBy
	 * @param updatedBy
	 */
	public OperationCommon(User createdBy, User updatedBy) {
		super();
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
	}

	/**
	 * @return the createdBy
	 */
	public User getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the updatedBy
	 */
	public User getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OperationCommon [createdBy=");
		builder.append(createdBy);
		builder.append(", updatedBy=");
		builder.append(updatedBy);
		builder.append("]");
		return builder.toString();
	}
}
