/**
 * 
 */
package com.cisbarey.proturvac.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Stratis
 *
 */
public class Profile extends Common {

	/**
	 * Generado por el IDE.
	 */
	private static final long serialVersionUID = 5226053724611660579L;
	
	private Integer id;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String description;
	
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private List<Menu> privileges;

	/**
	 * Constructor default.
	 */
	public Profile() {
		super();
	}

	/**
	 * @param id
	 * @param description
	 */
	public Profile(Integer id, String description) {
		super();
		this.id = id;
		this.description = description;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the privileges
	 */
	public List<Menu> getPrivileges() {
		return privileges;
	}

	/**
	 * @param privileges the privileges to set
	 */
	public void setPrivileges(List<Menu> privileges) {
		this.privileges = privileges;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Profile [id=");
		builder.append(id);
		builder.append(", description=");
		builder.append(description);
		builder.append(", privileges=");
		builder.append(privileges);
		builder.append("]");
		return builder.toString();
	}

}
