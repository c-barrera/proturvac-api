/**
 * 
 */
package com.cisbarey.proturvac.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Stratis
 *
 */
public class Office extends Common {

	/**
	 * Generado por el IDE.
	 */
	private static final long serialVersionUID = 2093435183965451564L;
	
	private Integer id;
	private String description;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String address;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private Double latitude;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private Double longitude;
	
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private Company company;

	/**
	 * Constructor default.
	 */
	public Office() {
		super();
	}

	/**
	 * @param id
	 * @param description
	 * @param address
	 * @param latitude
	 * @param longitude
	 * @param company
	 */
	public Office(Integer id, String description, String address, Double latitude, Double longitude, Company company) {
		super();
		this.id = id;
		this.description = description;
		this.address = address;
		this.latitude = latitude;
		this.longitude = longitude;
		this.company = company;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the latitude
	 */
	public Double getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public Double getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Office [id=");
		builder.append(id);
		builder.append(", description=");
		builder.append(description);
		builder.append(", address=");
		builder.append(address);
		builder.append(", latitude=");
		builder.append(latitude);
		builder.append(", longitude=");
		builder.append(longitude);
		builder.append(", company=");
		builder.append(company);
		builder.append("]");
		return builder.toString();
	}

}
