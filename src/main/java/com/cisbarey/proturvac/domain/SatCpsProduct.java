/**
 * 
 */
package com.cisbarey.proturvac.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author cisbarey
 *
 */
public class SatCpsProduct implements Serializable {

	/**
	 * Generado por el IDE.
	 */
	private static final long serialVersionUID = -8485888638305709982L;
	
	private Integer id;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String code;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String description;
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private Integer fkSatCpsClass;
	
	@JsonInclude(value = Include.NON_EMPTY, content = Include.NON_NULL)
	private String sku;

	/**
	 * Constructor default.
	 */
	public SatCpsProduct() {
		super();
	}

	/**
	 * @param id
	 * @param code
	 * @param description
	 * @param fkSatCpsClass
	 * @param sku
	 */
	public SatCpsProduct(Integer id, String code, String description, Integer fkSatCpsClass, String sku) {
		super();
		this.id = id;
		this.code = code;
		this.description = description;
		this.fkSatCpsClass = fkSatCpsClass;
		this.sku = sku;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the fkSatCpsClass
	 */
	public Integer getFkSatCpsClass() {
		return fkSatCpsClass;
	}

	/**
	 * @param fkSatCpsClass the fkSatCpsClass to set
	 */
	public void setFkSatCpsClass(Integer fkSatCpsClass) {
		this.fkSatCpsClass = fkSatCpsClass;
	}

	/**
	 * @return the sku
	 */
	public String getSku() {
		return sku;
	}

	/**
	 * @param sku the sku to set
	 */
	public void setSku(String sku) {
		this.sku = sku;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SatCpsProduct [id=");
		builder.append(id);
		builder.append(", code=");
		builder.append(code);
		builder.append(", description=");
		builder.append(description);
		builder.append(", fkSatCpsClass=");
		builder.append(fkSatCpsClass);
		builder.append(", sku=");
		builder.append(sku);
		builder.append("]");
		return builder.toString();
	}
}
