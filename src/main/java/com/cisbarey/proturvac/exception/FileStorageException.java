/**
 * 
 */
package com.cisbarey.proturvac.exception;

/**
 * @author cisbarey
 *
 */
public class FileStorageException extends RuntimeException {

	/**
	 * Generado por el IDE.
	 */
	private static final long serialVersionUID = 4353239269874444549L;

	public FileStorageException(String message) {
		super(message);
	}

	public FileStorageException(String message, Throwable cause) {
		super(message, cause);
	}

}
