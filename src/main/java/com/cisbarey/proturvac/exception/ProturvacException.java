/**
 * 
 */
package com.cisbarey.proturvac.exception;

/**
 * @author Stratis
 *
 */
public class ProturvacException extends Exception {

	/**
	 * Generado por el IDE.
	 */
	private static final long serialVersionUID = -6476480490393774858L;

	/**
	 * 
	 * @param message
	 * @param error
	 */
	public ProturvacException(String message) {
		super(message);
	}
}
