/**
 * 
 */
package com.cisbarey.proturvac.service;

import java.util.List;

import com.cisbarey.proturvac.domain.Menu;
import com.cisbarey.proturvac.exception.ProturvacException;

/**
 * <p>Clase que se encarga de realizar las llamadas a los mappers.</p>
 * @author Stratis
 *
 */
public interface IMenuService extends IService<Menu> {
	
	/**
	 * <p>M&eacute;todo que obtiene el listao del men&uacute; con respecto 
	 * al perfil de un usuario.</p>
	 * @param id El id del perfil.
	 * @return Listado con el men&uacute;.
	 * @throws ProturvacException
	 */
	List<Menu> getByProfile(int id) throws ProturvacException ;

	/**
	 * <p>M&eacute;todo que obtiene el listado de privilegios con respecto 
	 * al perfil de un usuario.</p>
	 * @param id El id del perfil.
	 * @return Listado de privilegios.
	 * @throws ProturvacException
	 */
	List<String> getPrivileges(int id) throws ProturvacException;
}
