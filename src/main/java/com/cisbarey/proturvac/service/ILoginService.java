/**
 * 
 */
package com.cisbarey.proturvac.service;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import com.cisbarey.proturvac.domain.Credentials;
import com.cisbarey.proturvac.domain.User;
import com.cisbarey.proturvac.exception.ProturvacException;

/**
 * @author Stratis
 *
 */
public interface ILoginService {
	
	User login(Credentials credentials) throws ProturvacException, NoSuchAlgorithmException, InvalidKeySpecException;

}
