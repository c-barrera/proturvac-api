/**
 * 
 */
package com.cisbarey.proturvac.service;

import java.io.IOException;
import java.util.Map;

import javax.mail.MessagingException;

import com.cisbarey.proturvac.domain.Mail;

import freemarker.template.TemplateException;

/**
 * @author Stratis
 *
 */
public interface IEmailService {
	
	void sendSimpleMessage(Mail mail) throws MessagingException, IOException, TemplateException;
	
	Mail buildMail(String to, String subject, Map<String, Object> model);

}
