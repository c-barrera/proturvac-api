/**
 * 
 */
package com.cisbarey.proturvac.service;

import java.util.List;

import com.cisbarey.proturvac.domain.Product;
import com.cisbarey.proturvac.exception.ProturvacException;

/**
 * <p>Clase que se encarga de realizar las llamadas a los mappers.</p>
 * @author Stratis
 *
 */
public interface IProductService extends IService<Product> {
	
	List<Product> getProductsByProvider(int id) throws ProturvacException;

}
