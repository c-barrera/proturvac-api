/**
 * 
 */
package com.cisbarey.proturvac.service;

import com.cisbarey.proturvac.domain.Company;

/**
 * <p>Clase que se encarga de realizar las llamadas a los mappers.</p>
 * @author Stratis
 *
 */
public interface ICompanyService extends IService<Company> {

}
