/**
 * 
 */
package com.cisbarey.proturvac.service;

import com.cisbarey.proturvac.domain.Privilege;
import com.cisbarey.proturvac.exception.ProturvacException;

/**
 * <p>Clase que se encarga de realizar las llamadas a los mappers.</p>
 * 
 * @author Stratis
 */
public interface IPrivilegeService {
	
	public String save(Privilege param) throws ProturvacException;
	
	public String delete(int profile, int menu) throws ProturvacException;

}
