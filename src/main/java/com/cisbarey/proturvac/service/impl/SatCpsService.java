/**
 * 
 */
package com.cisbarey.proturvac.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.cisbarey.proturvac.domain.SatCpsClass;
import com.cisbarey.proturvac.domain.SatCpsProduct;
import com.cisbarey.proturvac.domain.SearchSKU;
import com.cisbarey.proturvac.exception.ProturvacException;
import com.cisbarey.proturvac.mapper.ISatCpsClassMapper;
import com.cisbarey.proturvac.mapper.ISatCpsProductMapper;
import com.cisbarey.proturvac.service.ISatCpsService;
import com.cisbarey.proturvac.util.IConstants;

/**
 * @author cisbarey
 *
 */
@Service
public class SatCpsService implements ISatCpsService {
	
	@Autowired
	private ISatCpsClassMapper mapperClass;
	@Autowired
	private ISatCpsProductMapper mapperProduct;

	@Override
	public List<SatCpsClass> getList() throws ProturvacException {
		List<SatCpsClass> result = this.mapperClass.findList();

		if (result != null && !result.isEmpty()) {
			return result;
		}
		throw new ProturvacException(IConstants.Errors.LIST_EMPTY);
	}

	@Override
	public SatCpsClass getOne(int id) throws ProturvacException {
		SatCpsClass result = this.mapperClass.findOne(id);

		if (result != null) {
			return result;
		}
		throw new ProturvacException(IConstants.Errors.NOT_FOUND);
	}

	@Override
	public String save(SatCpsClass param) throws ProturvacException {
		try {
			this.mapperClass.insert(param);
			return IConstants.Success.INSERT;
		} catch (DuplicateKeyException e) {
			throw new ProturvacException(IConstants.Errors.DUPLICATE_KEY);
		}
	}

	@Override
	public String update(SatCpsClass param) throws ProturvacException {
		try {
			this.mapperClass.update(param);
			return IConstants.Success.UPDATE;
		} catch (DuplicateKeyException e) {
			throw new ProturvacException(IConstants.Errors.DUPLICATE_KEY);
		}
	}

	@Override
	public String delete(int id) throws ProturvacException {
		this.mapperClass.delete(id);
		return IConstants.Success.DELETE;
	}
	
	@Override
	public void chargeCFDIClass() throws ProturvacException {
		Workbook workbook = null;
		try {
			File resource = new ClassPathResource("templates/catCFDI.xls").getFile();
			workbook = WorkbookFactory.create(resource);
			Sheet sheet = workbook.getSheet("c_ClaveProdServ");
			Iterator<Row> rows = sheet.rowIterator();
			
			while(rows.hasNext()) {
				Row row = rows.next();
				
				if (row.getRowNum() > 3) {
					Cell cell = row.getCell(0);
					
					if (cell != null) {
						cell.setCellType(CellType.STRING);
						String value = cell.getStringCellValue();
						
						if (value != null && !value.isEmpty() && value.length() == 8 && value.endsWith("00")) {
							String division = value.substring(0, 2);
							String group = value.substring(2, 4);
							
							Map<String, String> params = new HashMap<>();
							params.put("division", division);
							params.put("group", group);
							Integer fkSatCpsGroup = this.mapperClass.findId(params);
							
							if (fkSatCpsGroup != null) {
								String clase = value.substring(4, 6);
								String description = row.getCell(1).getStringCellValue();
								
								SatCpsClass param = new SatCpsClass();
								param.setCode(clase);
								param.setDescription(description);
								param.setFkSatCpsGroup(fkSatCpsGroup);
								this.mapperClass.insert(param );
							} else {
								System.out.println(value);
							}
						}
					}
				}
			}
		} catch (IOException e) {
			throw new ProturvacException(e.getMessage());
		} catch (EncryptedDocumentException e) {
			throw new ProturvacException(e.getMessage());
		} catch (InvalidFormatException e) {
			throw new ProturvacException(e.getMessage());
		} finally {
			try {
				if (workbook != null) {
					workbook.close();
				}
			} catch (IOException e) {
				throw new ProturvacException(e.getMessage());
			}
		}
	}

	@Override
	public void chargeCFDIProduct() throws ProturvacException {
		Workbook workbook = null;
		try {
			File resource = new ClassPathResource("templates/catCFDI.xls").getFile();
			workbook = WorkbookFactory.create(resource);
			Sheet sheet = workbook.getSheet("c_ClaveProdServ");
			Iterator<Row> rows = sheet.rowIterator();
			
			while(rows.hasNext()) {
				Row row = rows.next();
				
				if (row.getRowNum() > 3) {
					Cell cell = row.getCell(0);
					
					if (cell != null) {
						cell.setCellType(CellType.STRING);
						String value = cell.getStringCellValue();
						
						if (value != null && !value.isEmpty() && value.length() == 8 && !value.endsWith("00")) {
							String division = value.substring(0, 2);
							String group = value.substring(2, 4);
							String clazz = value.substring(4, 6);
							
							Map<String, String> params = new HashMap<>();
							params.put("division", division);
							params.put("group", group);
							params.put("class", clazz);
							Integer fkSatCpsClass = this.mapperProduct.findId(params);
							
							if (fkSatCpsClass != null) {
								String product = value.substring(6, 8);
								String description = row.getCell(1).getStringCellValue();
								
								SatCpsProduct param = new SatCpsProduct();
								param.setCode(product);
								param.setDescription(description);
								param.setFkSatCpsClass(fkSatCpsClass);
								this.mapperProduct.insert(param);
							} else {
								System.out.println(value);
							}
						}
					}
				}
			}
		} catch (IOException e) {
			throw new ProturvacException(e.getMessage());
		} catch (EncryptedDocumentException e) {
			throw new ProturvacException(e.getMessage());
		} catch (InvalidFormatException e) {
			throw new ProturvacException(e.getMessage());
		} finally {
			try {
				if (workbook != null) {
					workbook.close();
				}
			} catch (IOException e) {
				throw new ProturvacException(e.getMessage());
			}
		}
	}

	@Override
	public List<SearchSKU> searchSKU(String text) throws ProturvacException {
		List<SearchSKU> result = this.mapperProduct.searchSKU(text);

		if (result != null && !result.isEmpty()) {
			return result;
		}
		throw new ProturvacException(IConstants.Errors.LIST_EMPTY);
	}

	@Override
	public SearchSKU getSKUByIdProduct(int id) throws ProturvacException {
		SearchSKU result = this.mapperProduct.findSKUByIdProduct(id);

		if (result != null) {
			return result;
		}
		throw new ProturvacException(IConstants.Errors.NOT_FOUND);
	}

}
