/**
 * 
 */
package com.cisbarey.proturvac.service.impl;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;

import com.cisbarey.proturvac.config.JWTAuthorizationFilter;
import com.cisbarey.proturvac.domain.Credentials;
import com.cisbarey.proturvac.domain.User;
import com.cisbarey.proturvac.exception.ProturvacException;
import com.cisbarey.proturvac.mapper.ILoginMapper;
import com.cisbarey.proturvac.mapper.IMenuMapper;
import com.cisbarey.proturvac.service.ILoginService;
import com.cisbarey.proturvac.util.IConstants;
import com.cisbarey.proturvac.util.IPasswordUtil;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * @author Stratis
 *
 */
@Service
public class LoginService implements ILoginService {

	@Autowired
	private ILoginMapper mapper;
	@Autowired
	private IMenuMapper menuMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cisbarey.proturvac.service.ILoginService#login(com.cisbarey.proturvac.
	 * domain.Credentials)
	 */
	@Override
	public User login(Credentials credentials)
			throws ProturvacException, NoSuchAlgorithmException, InvalidKeySpecException {
		User user = this.mapper.login(credentials.getUsername());

		if (user != null) {
			boolean matched = IPasswordUtil.Hash.validate(credentials.getPassword(), user.getPassword());

			if (matched) {
				String token = this.getJWTToken(user.getEmail(), user.getProfile().getId());
				user.setToken(token);
				return user;
			} else {
				throw new ProturvacException(IConstants.Errors.USER_INVALID);
			}
		} else {
			throw new ProturvacException(IConstants.Errors.USER_INVALID);
		}
	}

	private String getJWTToken(String username, int profileId) {
		String secretKey = JWTAuthorizationFilter.SECRET;
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils.createAuthorityList("PERMISSION_INVITE");
		List<String> privileges = this.menuMapper.findPrivileges(profileId);

		if (privileges != null && !privileges.isEmpty()) {
			int tam = privileges.size();
			String[] roles = new String[tam];
			for (int i = 0; i < tam; i++) {
				roles[i] = "PERMISSION_" + privileges.get(i);
			}
			grantedAuthorities = AuthorityUtils.createAuthorityList(roles);
		}

		String token = Jwts.builder().setId("proturvacJWT").setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000))
				.signWith(SignatureAlgorithm.HS512, secretKey.getBytes()).compact();

		return "Bearer " + token;
	}

}
