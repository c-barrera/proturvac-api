/**
 * 
 */
package com.cisbarey.proturvac.service.impl;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.cisbarey.proturvac.domain.User;
import com.cisbarey.proturvac.exception.ProturvacException;
import com.cisbarey.proturvac.mapper.IUserMapper;
import com.cisbarey.proturvac.service.IEmailService;
import com.cisbarey.proturvac.service.IUserService;
import com.cisbarey.proturvac.util.IConstants;
import com.cisbarey.proturvac.util.IPasswordUtil;

import freemarker.template.TemplateException;

/**
 * @author Stratis
 *
 */
@Service
public class UserService implements IUserService {

	@Autowired
	private IUserMapper mapper;
	@Autowired
	private IEmailService iEmailService;
	@Autowired
	private Environment environment;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cisbarey.proturvac.service.IService#getList(java.lang.Object)
	 */
	@Override
	public List<User> getList() throws ProturvacException {
		List<User> result = this.mapper.findList();

		if (result != null && !result.isEmpty()) {
			return result;
		}
		throw new ProturvacException(IConstants.Errors.LIST_EMPTY);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cisbarey.proturvac.service.IService#getOne(int)
	 */
	@Override
	public User getOne(int id) throws ProturvacException {
		User result = this.mapper.findOne(id);

		if (result != null) {
			return result;
		}
		throw new ProturvacException(IConstants.Errors.NOT_FOUND);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cisbarey.proturvac.service.IService#save(java.lang.Object)
	 */
	@Override
	public String save(User param) throws ProturvacException {
		try {
			String passwordRandom = IPasswordUtil.Random.generate();
			param.setPassword(IPasswordUtil.Hash.generate(passwordRandom));
			// TODO Validar si el correo electrónico es correto.
			this.mapper.insert(param);
			int id = this.mapper.findID(param.getEmail());
			param.setId(id);
			this.mapper.insertEmployee(param);
			Map<String, Object> model = new HashMap<>();
			model.put("firstName", param.getFirstName());
			model.put("lastName", param.getLastName());
			model.put("email", param.getEmail());
			model.put("password", passwordRandom);
			model.put("url", this.environment.getProperty("url.front"));

			this.iEmailService.sendSimpleMessage(this.iEmailService.buildMail(param.getEmail(),
					this.environment.getProperty("mail.user.new.subject"), model));
			return IConstants.Success.INSERT;
		} catch (DuplicateKeyException e) {
			throw new ProturvacException(IConstants.Errors.DUPLICATE_KEY);
		} catch (NoSuchAlgorithmException e) {
			throw new ProturvacException(e.getMessage());
		} catch (InvalidKeySpecException e) {
			throw new ProturvacException(e.getMessage());
		} catch (MessagingException e) {
			throw new ProturvacException(e.getMessage());
		} catch (IOException e) {
			throw new ProturvacException(e.getMessage());
		} catch (TemplateException e) {
			throw new ProturvacException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cisbarey.proturvac.service.IService#update(java.lang.Object)
	 */
	@Override
	public String update(User param) throws ProturvacException {
		try {
			this.mapper.update(param);
			return IConstants.Success.UPDATE;
		} catch (DuplicateKeyException e) {
			throw new ProturvacException(IConstants.Errors.DUPLICATE_KEY);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cisbarey.proturvac.service.IService#delete(int)
	 */
	@Override
	public String delete(int id) throws ProturvacException {
		this.mapper.deleteEmployee(id);
		this.mapper.delete(id);
		return IConstants.Success.DELETE;
	}
}
