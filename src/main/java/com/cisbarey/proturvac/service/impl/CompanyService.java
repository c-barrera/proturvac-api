/**
 * 
 */
package com.cisbarey.proturvac.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.cisbarey.proturvac.domain.Company;
import com.cisbarey.proturvac.exception.ProturvacException;
import com.cisbarey.proturvac.mapper.ICompanyMapper;
import com.cisbarey.proturvac.service.ICompanyService;
import com.cisbarey.proturvac.util.IConstants;

/**
 * @author Stratis
 *
 */
@Service
public class CompanyService implements ICompanyService {
	
	@Autowired
	private ICompanyMapper mapper;

	/* (non-Javadoc)
	 * @see com.cisbarey.proturvac.service.IService#getList()
	 */
	@Override
	public List<Company> getList() throws ProturvacException {
		List<Company> result = this.mapper.findList();

		if (result != null && !result.isEmpty()) {
			return result;
		}
		throw new ProturvacException(IConstants.Errors.LIST_EMPTY);
	}

	/* (non-Javadoc)
	 * @see com.cisbarey.proturvac.service.IService#getOne(int)
	 */
	@Override
	public Company getOne(int id) throws ProturvacException {
		Company result = this.mapper.findOne(id);

		if (result != null) {
			return result;
		}
		throw new ProturvacException(IConstants.Errors.NOT_FOUND);
	}

	/* (non-Javadoc)
	 * @see com.cisbarey.proturvac.service.IService#save(java.lang.Object)
	 */
	@Override
	public String save(Company param) throws ProturvacException {
		try {
			this.mapper.insert(param);
			return IConstants.Success.INSERT;
		} catch (DuplicateKeyException e) {
			throw new ProturvacException(IConstants.Errors.DUPLICATE_KEY);
		}
	}

	/* (non-Javadoc)
	 * @see com.cisbarey.proturvac.service.IService#update(java.lang.Object)
	 */
	@Override
	public String update(Company param) throws ProturvacException {
		try {
			this.mapper.update(param);
			return IConstants.Success.UPDATE;
		} catch (DuplicateKeyException e) {
			throw new ProturvacException(IConstants.Errors.DUPLICATE_KEY);
		}
	}

	/* (non-Javadoc)
	 * @see com.cisbarey.proturvac.service.IService#delete(int)
	 */
	@Override
	public String delete(int id) throws ProturvacException {
		this.mapper.delete(id);
		return IConstants.Success.DELETE;
	}

}
