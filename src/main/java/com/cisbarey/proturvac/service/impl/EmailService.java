/**
 * 
 */
package com.cisbarey.proturvac.service.impl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.cisbarey.proturvac.domain.Mail;
import com.cisbarey.proturvac.service.IEmailService;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * @author Stratis
 *
 */
@Service
public class EmailService implements IEmailService {

	@Autowired
	private JavaMailSender emailSender;

	@Autowired
	private Configuration freemarkerConfig;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cisbarey.proturvac.service.IEmailService#sendSimpleMessage(com.cisbarey.
	 * proturvac.domain.Mail)
	 */
	@Override
	public void sendSimpleMessage(Mail mail) throws MessagingException, IOException, TemplateException {
		MimeMessage message = this.emailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
				StandardCharsets.UTF_8.name());

		Template t = this.freemarkerConfig.getTemplate("email.ftl");
		String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, mail.getModel());

		helper.setTo(mail.getTo());
		helper.setText(html, true);
		helper.setSubject(mail.getSubject());
		helper.setFrom(mail.getFrom());

		this.emailSender.send(message);
	}

	@Override
	public Mail buildMail(String to, String subject, Map<String, Object> model) {
		Mail mail = new Mail();
        mail.setFrom("no-reply@proturvac.com");
        mail.setTo(to);
        mail.setSubject(subject);
        mail.setModel(model);
        return mail;
	}

}
