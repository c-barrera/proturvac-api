/**
 * 
 */
package com.cisbarey.proturvac.service.impl;

import java.io.File;
import java.text.MessageFormat;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.cisbarey.proturvac.domain.Product;
import com.cisbarey.proturvac.exception.ProturvacException;
import com.cisbarey.proturvac.mapper.IProductMapper;
import com.cisbarey.proturvac.service.IProductService;
import com.cisbarey.proturvac.util.IConstants;

/**
 * @author Stratis
 *
 */
@Service
public class ProductService implements IProductService {

	@Autowired
	private IProductMapper mapper;
	@Autowired
	private StorageService storage;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cisbarey.proturvac.service.IService#getList()
	 */
	@Override
	public List<Product> getList() throws ProturvacException {
		List<Product> result = this.mapper.findList();

		if (result != null && !result.isEmpty()) {
			return result;
		}
		throw new ProturvacException(IConstants.Errors.LIST_EMPTY);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cisbarey.proturvac.service.IService#getOne(int)
	 */
	@Override
	public Product getOne(int id) throws ProturvacException {
		Product result = this.mapper.findOne(id);

		if (result != null) {
			return result;
		}
		throw new ProturvacException(IConstants.Errors.NOT_FOUND);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cisbarey.proturvac.service.IService#save(java.lang.Object)
	 */
	@Override
	public String save(Product param) throws ProturvacException {
		try {
			this.saveOrUpdate(param, ActionCommand.INSERT);
			return IConstants.Success.INSERT;
		} catch (DuplicateKeyException e) {
			throw new ProturvacException(IConstants.Errors.DUPLICATE_KEY);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cisbarey.proturvac.service.IService#update(java.lang.Object)
	 */
	@Override
	public String update(Product param) throws ProturvacException {
		try {
			this.saveOrUpdate(param, ActionCommand.UPDATE);
			return IConstants.Success.UPDATE;
		} catch (DuplicateKeyException e) {
			throw new ProturvacException(IConstants.Errors.DUPLICATE_KEY);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cisbarey.proturvac.service.IService#delete(int)
	 */
	@Override
	public String delete(int id) throws ProturvacException {
		this.deleteImageProduct(id);
		this.mapper.delete(id);
		return IConstants.Success.DELETE;
	}
	
	private void deleteImageProduct(int id) {
		Product product = this.mapper.findOne(id);

		if (product != null && !product.getImage().equals("default.png")) {
			String fullName = MessageFormat.format(IConstants.File.FULL_NAME_PRODUCT_PATTERN, File.separator,
					product.getImage());
			this.storage.delete(fullName);
		}
	}

	private void saveOrUpdate(Product param, ActionCommand action) throws ProturvacException {
		if (param.getFile() != null) {
			String originalFileName = param.getFile().getOriginalFilename();

			if (originalFileName != null && !originalFileName.isEmpty()) {
				String[] ungroupName = originalFileName.split("\\.");

				if (ungroupName != null && ungroupName.length > 0) {
					String extension = ungroupName[ungroupName.length - 1];
					String fileName = MessageFormat.format(IConstants.File.FILE_NAME_PRODUCT_PATTERN,
							UUID.randomUUID().toString(), extension);

					if (action == ActionCommand.UPDATE) {
						this.deleteImageProduct(param.getId());
					}

					param.setImage(fileName);
					this.executeCommand(action, param);

					String fullName = MessageFormat.format(IConstants.File.FULL_NAME_PRODUCT_PATTERN, File.separator,
							fileName);
					this.storage.store(param.getFile(), fullName);
				} else {
					throw new ProturvacException("El archivo seleccionado no tiene extensión.");
				}
			} else {
				throw new ProturvacException("No se encontró un nombre para el archivo seleccionado.");
			}
		} else {
			this.executeCommand(action, param);
		}
	}

	private void executeCommand(ActionCommand action, Product param) {
		switch (action) {
		case INSERT:
			this.mapper.insert(param);
			break;
		case UPDATE:
			this.mapper.update(param);
			break;
		default:

			break;
		}
	}

	private enum ActionCommand {
		INSERT, UPDATE
	}

	@Override
	public List<Product> getProductsByProvider(int id) throws ProturvacException {
		List<Product> result = this.mapper.findProductsByProvider(id);
		if (result != null && !result.isEmpty()) {
			return result;
		}
		throw new ProturvacException(IConstants.Errors.LIST_EMPTY);
	}

}
