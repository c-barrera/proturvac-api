package com.cisbarey.proturvac.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.cisbarey.proturvac.domain.Privilege;
import com.cisbarey.proturvac.exception.ProturvacException;
import com.cisbarey.proturvac.mapper.IPrivilegeMapper;
import com.cisbarey.proturvac.service.IPrivilegeService;
import com.cisbarey.proturvac.util.IConstants;

@Service
public class PrivilegeService implements IPrivilegeService {
	
	@Autowired
	private IPrivilegeMapper mapper;

	@Override
	public String save(Privilege param) throws ProturvacException {
		try {
			this.mapper.insert(param);
			return IConstants.Success.INSERT;
		} catch (DuplicateKeyException e) {
			throw new ProturvacException(IConstants.Errors.DUPLICATE_KEY);
		}
	}

	@Override
	public String delete(int profile, int menu) throws ProturvacException {
		Privilege param = new Privilege(profile, menu);
		this.mapper.delete(param);
		return IConstants.Success.DELETE;
	}

}
