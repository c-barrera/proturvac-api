/**
 * 
 */
package com.cisbarey.proturvac.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.cisbarey.proturvac.domain.Menu;
import com.cisbarey.proturvac.exception.ProturvacException;
import com.cisbarey.proturvac.mapper.IMenuMapper;
import com.cisbarey.proturvac.service.IMenuService;
import com.cisbarey.proturvac.util.IConstants;

/**
 * @author Stratis
 *
 */
@Service
public class MenuService implements IMenuService {

	@Autowired
	private IMenuMapper mapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cisbarey.proturvac.service.IMenuService#getByProfile(int)
	 */
	@Override
	public List<Menu> getByProfile(int id) throws ProturvacException {
		List<Menu> result = this.mapper.findByProfile(id);

		if (result != null && !result.isEmpty()) {
			return result;
		}
		throw new ProturvacException(IConstants.Errors.LIST_EMPTY);
	}

	@Override
	public List<String> getPrivileges(int id) throws ProturvacException {
		List<String> result = this.mapper.findPrivileges(id);

		if (result != null && !result.isEmpty()) {
			return result;
		}
		throw new ProturvacException(IConstants.Errors.LIST_EMPTY);
	}

	@Override
	public List<Menu> getList() throws ProturvacException {
		List<Menu> result = this.mapper.findList();

		if (result != null && !result.isEmpty()) {
			return result;
		}
		throw new ProturvacException(IConstants.Errors.LIST_EMPTY);
	}

	@Override
	public Menu getOne(int id) throws ProturvacException {
		Menu result = this.mapper.findOne(id);

		if (result != null) {
			return result;
		}
		throw new ProturvacException(IConstants.Errors.NOT_FOUND);
	}

	@Override
	public String save(Menu param) throws ProturvacException {
		try {
			this.mapper.insert(param);
			return IConstants.Success.INSERT;
		} catch (DuplicateKeyException e) {
			throw new ProturvacException(IConstants.Errors.DUPLICATE_KEY);
		}
	}

	@Override
	public String update(Menu param) throws ProturvacException {
		try {
			this.mapper.update(param);
			return IConstants.Success.UPDATE;
		} catch (DuplicateKeyException e) {
			throw new ProturvacException(IConstants.Errors.DUPLICATE_KEY);
		}
	}

	@Override
	public String delete(int id) throws ProturvacException {
		this.mapper.delete(id);
		return IConstants.Success.DELETE;
	}

}
