/**
 * 
 */
package com.cisbarey.proturvac.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.cisbarey.proturvac.domain.Office;
import com.cisbarey.proturvac.exception.ProturvacException;
import com.cisbarey.proturvac.mapper.IOfficeMapper;
import com.cisbarey.proturvac.service.IOfficeService;
import com.cisbarey.proturvac.util.IConstants;

/**
 * @author Stratis
 *
 */
@Service
public class OfficeService implements IOfficeService {

	@Autowired
	private IOfficeMapper mapper;

	/* (non-Javadoc)
	 * @see com.cisbarey.proturvac.service.IService#getList()
	 */
	@Override
	public List<Office> getList() throws ProturvacException {
		List<Office> result = this.mapper.findList();

		if (result != null && !result.isEmpty()) {
			return result;
		}
		throw new ProturvacException(IConstants.Errors.LIST_EMPTY);
	}

	/* (non-Javadoc)
	 * @see com.cisbarey.proturvac.service.IService#getOne(int)
	 */
	@Override
	public Office getOne(int id) throws ProturvacException {
		Office result = this.mapper.findOne(id);

		if (result != null) {
			return result;
		}
		throw new ProturvacException(IConstants.Errors.NOT_FOUND);
	}

	/* (non-Javadoc)
	 * @see com.cisbarey.proturvac.service.IService#save(java.lang.Object)
	 */
	@Override
	public String save(Office param) throws ProturvacException {
		try {
			this.mapper.insert(param);
			return IConstants.Success.INSERT;
		} catch (DuplicateKeyException e) {
			throw new ProturvacException(IConstants.Errors.DUPLICATE_KEY);
		}
	}

	/* (non-Javadoc)
	 * @see com.cisbarey.proturvac.service.IService#update(java.lang.Object)
	 */
	@Override
	public String update(Office param) throws ProturvacException {
		try {
			this.mapper.update(param);
			return IConstants.Success.UPDATE;
		} catch (DuplicateKeyException e) {
			throw new ProturvacException(IConstants.Errors.DUPLICATE_KEY);
		}
	}

	/* (non-Javadoc)
	 * @see com.cisbarey.proturvac.service.IService#delete(int)
	 */
	@Override
	public String delete(int id) throws ProturvacException {
		this.mapper.delete(id);
		return IConstants.Success.DELETE;
	}

	@Override
	public List<Office> getByCompany(int company) throws ProturvacException {
		List<Office> result = this.mapper.findByCompany(company);

		if (result != null && !result.isEmpty()) {
			return result;
		}
		throw new ProturvacException(IConstants.Errors.LIST_EMPTY);
	}

}
