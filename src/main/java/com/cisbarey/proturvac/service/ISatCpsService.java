/**
 * 
 */
package com.cisbarey.proturvac.service;

import java.util.List;

import com.cisbarey.proturvac.domain.SatCpsClass;
import com.cisbarey.proturvac.domain.SearchSKU;
import com.cisbarey.proturvac.exception.ProturvacException;

/**
 * <p>Clase que se encarga de realizar las llamadas a los mappers.</p>
 * @author cisbarey
 *
 */
public interface ISatCpsService extends IService<SatCpsClass> {
	
	/**
	 * <p>M&eacute;todo que carga el nivel de clases desde el archivo CFDI del cat&aacute;logo de SAT.</p>
	 * @throws ProturvacException
	 */
	void chargeCFDIClass() throws ProturvacException;

	/**
	 * <p>M&eacute;todo que carga el nivel de productos desde el archivo CFDI del cat&aacute;logo de SAT.</p>
	 * @throws ProturvacException
	 */
	void chargeCFDIProduct() throws ProturvacException;
	
	/**
	 * 
	 * @param text
	 * @return
	 * @throws ProturvacException
	 */
	List<SearchSKU> searchSKU(String text) throws ProturvacException;
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws ProturvacException
	 */
	SearchSKU getSKUByIdProduct(int id) throws ProturvacException;
}
