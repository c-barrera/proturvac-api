/**
 * 
 */
package com.cisbarey.proturvac.service;

import com.cisbarey.proturvac.domain.User;

/**
 * <p>Clase que se encarga de realizar las llamadas a los mappers.</p>
 * @author Stratis
 *
 */
public interface IUserService extends IService<User> {

}
