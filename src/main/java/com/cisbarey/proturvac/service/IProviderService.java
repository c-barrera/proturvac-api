/**
 * 
 */
package com.cisbarey.proturvac.service;

import com.cisbarey.proturvac.domain.Provider;

/**
 * <p>Clase que se encarga de realizar las llamadas a los mappers.</p>
 * @author Stratis
 *
 */
public interface IProviderService extends IService<Provider> {

}
