/**
 * 
 */
package com.cisbarey.proturvac.service;

import java.util.List;

import com.cisbarey.proturvac.exception.ProturvacException;

/**
 * @author Stratis
 *
 */
public interface IService<T> {

	public List<T> getList() throws ProturvacException;
	public T getOne(int id) throws ProturvacException;
	public String save(T param) throws ProturvacException;
	public String update(T param) throws ProturvacException;
	public String delete(int id) throws ProturvacException;
}
