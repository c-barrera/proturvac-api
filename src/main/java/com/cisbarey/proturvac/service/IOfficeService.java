/**
 * 
 */
package com.cisbarey.proturvac.service;

import java.util.List;

import com.cisbarey.proturvac.domain.Office;
import com.cisbarey.proturvac.exception.ProturvacException;

/**
 * <p>Clase que se encarga de realizar las llamadas a los mappers.</p>
 * @author Stratis
 *
 */
public interface IOfficeService extends IService<Office> {

	List<Office> getByCompany(int company) throws ProturvacException;
}
