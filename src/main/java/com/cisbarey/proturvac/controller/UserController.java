/**
 * 
 */
package com.cisbarey.proturvac.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cisbarey.proturvac.domain.User;
import com.cisbarey.proturvac.exception.ProturvacException;
import com.cisbarey.proturvac.service.IUserService;

/**
 * @author Stratis
 *
 */
@RestController
@RequestMapping("/user")
public class UserController extends Controller {
	
	private static final Logger logger = LogManager.getLogger(UserController.class);
	
	@Autowired
	private IUserService service;

	/**
	 * Constructor default.
	 */
	public UserController() {
		super();
	}
	
	@GetMapping
	@PreAuthorize("hasPermission('permission', 'user')")
	public ResponseEntity<List<User>> getList() {
		try {
			return ResponseEntity.ok(this.service.getList());
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}
	
	@GetMapping(value = "/{id}")
	@PreAuthorize("hasPermission('permission', 'user')")
	public ResponseEntity<User> getOne(@PathVariable int id) {
		try {
			return ResponseEntity.ok(this.service.getOne(id));
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}

	@PostMapping
	@PreAuthorize("hasPermission('permission', 'user')")
	public <T> ResponseEntity<T> save(@RequestBody User user) {
		try {
			return super.getResponseOk(this.service.save(user));
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}

	@PutMapping(value = "/{id}")
	@PreAuthorize("hasPermission('permission', 'user')")
	public <T> ResponseEntity<T> update(@PathVariable int id, @RequestBody User user) {
		try {
			user.setId(id);
			return super.getResponseOk(this.service.update(user));
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}

	@DeleteMapping(value = "/{id}")
	@PreAuthorize("hasPermission('permission', 'user')")
	public <T> ResponseEntity<T> delete(@PathVariable int id) {
		try {
			return super.getResponseOk(this.service.delete(id));
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}

}
