/**
 * 
 */
package com.cisbarey.proturvac.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cisbarey.proturvac.domain.Product;
import com.cisbarey.proturvac.domain.SearchSKU;
import com.cisbarey.proturvac.exception.ProturvacException;
import com.cisbarey.proturvac.service.IProductService;
import com.cisbarey.proturvac.service.ISatCpsService;

/**
 * @author Stratis
 *
 */
@RestController
@RequestMapping("/product")
public class ProductController extends Controller {

	private static final Logger logger = LogManager.getLogger(ProductController.class);

	@Autowired
	private IProductService service;
	@Autowired
	private ISatCpsService iSatCpsService;

	/**
	 * Constructor default.
	 */
	public ProductController() {
		super();
	}

	@GetMapping
	@PreAuthorize("hasPermission('permission', 'product')")
	public ResponseEntity<List<Product>> getAll() {
		try {
			return ResponseEntity.ok(this.service.getList());
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}

	@GetMapping(value = "/{id}")
	@PreAuthorize("hasPermission('permission', 'product')")
	public ResponseEntity<Product> getById(@PathVariable int id) {
		try {
			return ResponseEntity.ok(this.service.getOne(id));
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}

	@PostMapping
	@PreAuthorize("hasPermission('permission', 'product')")
	public <T> ResponseEntity<T> save(@ModelAttribute Product data) {
		try {
			return super.getResponseOk(this.service.save(data));
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}

	@PutMapping(value = "/{id}")
	@PreAuthorize("hasPermission('permission', 'product')")
	public <T> ResponseEntity<T> update(@PathVariable int id, @ModelAttribute Product data) {
		try {
			data.setId(id);
			return super.getResponseOk(this.service.update(data));
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}

	@DeleteMapping(value = "/{id}")
	@PreAuthorize("hasPermission('permission', 'product')")
	public <T> ResponseEntity<T> delete(@PathVariable int id) {
		try {
			return super.getResponseOk(this.service.delete(id));
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}

	@GetMapping(value = "/search/sku/{text}")
	@PreAuthorize("hasPermission('permission', 'product')")
	public ResponseEntity<List<SearchSKU>> search(@PathVariable String text) {
		try {
			return ResponseEntity.ok(this.iSatCpsService.searchSKU(text));
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}

	@GetMapping(value = "/search/sku/id/{id}")
	@PreAuthorize("hasPermission('permission', 'product')")
	public ResponseEntity<SearchSKU> getSKUByIdProduct(@PathVariable int id) {
		try {
			return ResponseEntity.ok(this.iSatCpsService.getSKUByIdProduct(id));
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}

	@GetMapping(value = "/provider/{id}")
	@PreAuthorize("hasPermission('permission', 'product')")
	public ResponseEntity<List<Product>> getAllByProvider(@PathVariable int id) {
		try {
			return ResponseEntity.ok(this.service.getProductsByProvider(id));
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}
}
