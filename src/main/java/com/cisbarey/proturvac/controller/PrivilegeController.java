/**
 * 
 */
package com.cisbarey.proturvac.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cisbarey.proturvac.domain.Privilege;
import com.cisbarey.proturvac.exception.ProturvacException;
import com.cisbarey.proturvac.service.IPrivilegeService;

/**
 * @author Stratis
 *
 */
@RestController
@RequestMapping("/privilege")
public class PrivilegeController extends Controller {

	private static final Logger logger = LogManager.getLogger(PrivilegeController.class);

	@Autowired
	private IPrivilegeService service;
	
	@PostMapping
	@PreAuthorize("hasPermission('permission', 'menu')")
	public <T> ResponseEntity<T> save(@RequestBody Privilege data) {
		try {
			return super.getResponseOk(this.service.save(data));
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}

	@DeleteMapping(value = "/{profile}/{menu}")
	@PreAuthorize("hasPermission('permission', 'menu')")
	public <T> ResponseEntity<T> delete(@PathVariable int profile, @PathVariable int menu) {
		try {
			return super.getResponseOk(this.service.delete(profile, menu));
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}

}
