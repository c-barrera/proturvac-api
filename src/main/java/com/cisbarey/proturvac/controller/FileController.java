/**
 * 
 */
package com.cisbarey.proturvac.controller;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cisbarey.proturvac.service.impl.StorageService;
import com.cisbarey.proturvac.util.IConstants;

/**
 * @author cisbarey
 *
 */
@RestController
@RequestMapping("/download")
public class FileController {

	private static final Logger logger = LogManager.getLogger(FileController.class);

	@Autowired
	private StorageService storage;

	@GetMapping(value = "/product/image/{fileName:.+}")
	public ResponseEntity<Resource> getProductImage(@PathVariable String fileName, HttpServletRequest request) {
		// Load file as Resource
		String fullName = MessageFormat.format(IConstants.File.FULL_NAME_PRODUCT_PATTERN, File.separator, fileName);
		Resource resource = this.storage.load(fullName);

		// Try to determine file's content type
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (IOException ex) {
			logger.info("Could not determine file type.");
		}

		// Fallback to the default content type if type could not be determined
		if (contentType == null) {
			contentType = "application/octet-stream";
		}

		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}

}
