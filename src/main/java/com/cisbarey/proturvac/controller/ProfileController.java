/**
 * 
 */
package com.cisbarey.proturvac.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cisbarey.proturvac.domain.Profile;
import com.cisbarey.proturvac.exception.ProturvacException;
import com.cisbarey.proturvac.service.IProfileService;

/**
 * @author Stratis
 *
 */
@RestController
@RequestMapping("/profile")
public class ProfileController extends Controller {

	private static final Logger logger = LogManager.getLogger(ProfileController.class);
	
	@Autowired
	private IProfileService service;

	/**
	 * Constructor default.
	 */
	public ProfileController() {
		super();
	}

	@GetMapping
	@PreAuthorize("hasPermission('permission', 'profile')")
	public ResponseEntity<List<Profile>> getAll() {
		try {
			return ResponseEntity.ok(this.service.getList());
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}
	
	@GetMapping(value = "/{id}")
	@PreAuthorize("hasPermission('permission', 'profile')")
	public ResponseEntity<Profile> getById(@PathVariable int id) {
		try {
			return ResponseEntity.ok(this.service.getOne(id));
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}

	@PostMapping
	@PreAuthorize("hasPermission('permission', 'profile')")
	public <T> ResponseEntity<T> save(@RequestBody Profile data) {
		try {
			return super.getResponseOk(this.service.save(data));
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}

	@PutMapping(value = "/{id}")
	@PreAuthorize("hasPermission('permission', 'profile')")
	public <T> ResponseEntity<T> update(@PathVariable int id, @RequestBody Profile data) {
		try {
			data.setId(id);
			return super.getResponseOk(this.service.update(data));
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}

	@DeleteMapping(value = "/{id}")
	@PreAuthorize("hasPermission('permission', 'profile')")
	public <T> ResponseEntity<T> delete(@PathVariable int id) {
		try {
			return super.getResponseOk(this.service.delete(id));
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}
}
