/**
 * 
 */
package com.cisbarey.proturvac.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cisbarey.proturvac.domain.Menu;
import com.cisbarey.proturvac.exception.ProturvacException;
import com.cisbarey.proturvac.service.IMenuService;

/**
 * @author Stratis
 *
 */
@RestController
@RequestMapping("/menu")
public class MenuController extends Controller {

	private static final Logger logger = LogManager.getLogger(MenuController.class);
	
	@Autowired
	private IMenuService service;

	/**
	 * Constructor default.
	 */
	public MenuController() {
		super();
	}

	@GetMapping(value = "/profile/{id}")
	public ResponseEntity<List<Menu>> getByProfile(@PathVariable int id) {
		try {
			return ResponseEntity.ok(this.service.getByProfile(id));
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}
	
	@GetMapping
	@PreAuthorize("hasPermission('permission', 'menu')")
	public ResponseEntity<List<Menu>> getAll() {
		try {
			return ResponseEntity.ok(this.service.getList());
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}
	
	@GetMapping(value = "/{id}")
	@PreAuthorize("hasPermission('permission', 'menu')")
	public ResponseEntity<Menu> getById(@PathVariable int id) {
		try {
			return ResponseEntity.ok(this.service.getOne(id));
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}

	@PostMapping
	@PreAuthorize("hasPermission('permission', 'menu')")
	public <T> ResponseEntity<T> save(@RequestBody Menu menu) {
		try {
			return super.getResponseOk(this.service.save(menu));
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}

	@PutMapping(value = "/{id}")
	@PreAuthorize("hasPermission('permission', 'menu')")
	public <T> ResponseEntity<T> update(@PathVariable int id, @RequestBody Menu menu) {
		try {
			menu.setId(id);
			return super.getResponseOk(this.service.update(menu));
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}

	@DeleteMapping(value = "/{id}")
	@PreAuthorize("hasPermission('permission', 'menu')")
	public <T> ResponseEntity<T> delete(@PathVariable int id) {
		try {
			return super.getResponseOk(this.service.delete(id));
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}

}
