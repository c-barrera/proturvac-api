/**
 * 
 */
package com.cisbarey.proturvac.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.cisbarey.proturvac.exception.ProturvacException;

/**
 * @author Stratis
 *
 */
public class Controller {

	/**
	 * Constructor default.
	 */
	public Controller() {
		super();
	}
	
	@SuppressWarnings("unchecked")
	protected <T> ResponseEntity<T> getResponseOk(String message) {
		JsonObject ok = new JsonObject(message);
		return (ResponseEntity<T>) ResponseEntity.ok().body(ok);
	}
	
	@SuppressWarnings("unchecked")
	protected <T> ResponseEntity<T> getResponseError(ProturvacException e) {
		JsonObject error = new JsonObject(e.getMessage());
		return (ResponseEntity<T>) ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
	}
	
	@SuppressWarnings("unchecked")
	protected <T> ResponseEntity<T> getResponseError(Exception e) {
		JsonObject error = new JsonObject(e.getMessage());
		return (ResponseEntity<T>) ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body(error);
	}
	
	private class JsonObject {
		private String message;

		/**
		 * @param message
		 */
		public JsonObject(String message) {
			super();
			this.message = message;
		}

		/**
		 * @return the message
		 */
		@SuppressWarnings("unused")
		public String getMessage() {
			return message;
		}

		/**
		 * @param message the message to set
		 */
		@SuppressWarnings("unused")
		public void setMessage(String message) {
			this.message = message;
		}
	}

}
