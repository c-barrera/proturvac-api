/**
 * 
 */
package com.cisbarey.proturvac.controller;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cisbarey.proturvac.domain.Credentials;
import com.cisbarey.proturvac.domain.User;
import com.cisbarey.proturvac.exception.ProturvacException;
import com.cisbarey.proturvac.service.ILoginService;

/**
 * @author Stratis
 *
 */
@RestController
@RequestMapping("/login")
public class LoginController extends Controller {

	private static final Logger logger = LogManager.getLogger(LoginController.class);
	
	@Autowired
	private ILoginService service;

	/**
	 * Constructor default.
	 */
	public LoginController() {
		super();
	}

	@PostMapping
	public ResponseEntity<User> login(@RequestBody Credentials credentials) {
		try {
			return ResponseEntity.ok(this.service.login(credentials));
		} catch (ProturvacException | NoSuchAlgorithmException | InvalidKeySpecException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}

}
