/**
 * 
 */
package com.cisbarey.proturvac.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cisbarey.proturvac.exception.ProturvacException;
import com.cisbarey.proturvac.service.ISatCpsService;

/**
 * @author cisbarey
 *
 */
@RestController
@RequestMapping("/cfdi")
public class CFDIController extends Controller {

private static final Logger logger = LogManager.getLogger(CFDIController.class);
	
	@Autowired
	private ISatCpsService service;

	/**
	 * Constructor default.
	 */
	public CFDIController() {
		super();
	}

	@PostMapping(value = "/class")
	public ResponseEntity<String> chargeCFDIClass() {
		try {
			this.service.chargeCFDIClass();
			return ResponseEntity.ok("OK");
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}
	
	@PostMapping(value = "/product")
	public ResponseEntity<String> chargeCFDIProduct() {
		try {
			this.service.chargeCFDIProduct();
			return ResponseEntity.ok("OK");
		} catch (ProturvacException e) {
			logger.error(e.getMessage());
			return super.getResponseError(e);
		}
	}

}
